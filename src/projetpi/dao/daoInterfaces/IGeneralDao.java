/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.dao.daoInterfaces;

import java.util.List;

/**
 *
 * @author amin
 */
public interface IGeneralDao <T,I> {
    
    void add(T t);
    void update(T t);
    void delete(T t);
    T findById(I i);
    List<T> getAll(T t);
}
