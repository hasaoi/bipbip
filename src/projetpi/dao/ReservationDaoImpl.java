/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import projetpi.dao.daoInterfaces.IResevationDao;
import projetpi.entity.Reservation;
import projetpi.util.MyDB;





/**
 *
 * @author USER
 */
public class ReservationDaoImpl implements IResevationDao{
       private Connection cnx;

    @Override
    public void add(Reservation t) {
        
            try {
            cnx = MyDB.getInstance().getCnx();

            String req = "INSERT INTO `reservation`(`id_voyage`, `id_user`, `date_creation`, `etat`, `nb_place_resv`)"
                    + " VALUES ( ?, ? ,? ,?,?) ";
            PreparedStatement pst = cnx.prepareStatement(req);
            pst.setInt(1, t.getId_voyage());
            pst.setInt(2, t.getId_user());
            pst.setTimestamp(3, t.getDate_creation());
            pst.setInt(4, t.getEtat());
            pst.setInt(5, t.getNb_place_des());

            System.out.println(pst);
            //pst.executeUpdate();
            
            int i = pst.executeUpdate();
                    
		    if (i != 0) {
		        System.out.println("Reservation ajouté avec success");
		    } else {
		        System.out.println("Operation non aboutie");
		    }
        } catch (SQLException ex) {
           ex.printStackTrace();
        }

            
    }

    @Override
    public void update(Reservation t) {
            cnx = MyDB.getInstance().getCnx();

        String query="UPDATE `reservation` SET `nb_place_resv`=? WHERE `id_resv`=?";
		
		try {
   PreparedStatement pst = cnx.prepareStatement(query);
         pst.setInt(2,t.getId_rec());
	 pst.setInt(1,t.getNb_place_des());		
			pst.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
        
    }

    @Override
    public void delete(Reservation t) {
     cnx = MyDB.getInstance().getCnx();  
    String query=" DELETE FROM `reservation` WHERE `id_resv`=?" ;
		try {
   PreparedStatement pst = cnx.prepareStatement(query);
   pst.setInt(1,t.getId_rec());
			
			pst.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
    }

    @Override
    public Reservation findById(Integer i) {
Reservation res =new Reservation();
		String query="select * from `reservation` WHERE `id_resv`=?";
                cnx = MyDB.getInstance().getCnx();
		try {
                         PreparedStatement pst = cnx.prepareStatement(query);
			pst.setInt(1,i);
			ResultSet rs=pst.executeQuery();
			while(rs.next()) {
                           res.setId_rec(rs.getInt(1));
			   res.setId_voyage(rs.getInt(2));
			   res.setId_user(rs.getInt(3));
			   res.setEtat(rs.getInt(5));
			   res.setDate_creation(rs.getTimestamp(4));
                           res.setNb_place_des(rs.getInt(6));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return res;   
    }

    @Override
    public List<Reservation> getAll(Reservation t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}