/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import projetpi.dao.daoInterfaces.IUserDao;
import projetpi.entity.User;
import projetpi.enums.TypeSexe;
import projetpi.util.MyDB;

/**
 *
 * @author amin
 */
public class UserDaoImpl implements IUserDao{

    Connection cnx = MyDB.getInstance().getCnx();
    public void add(User t) {
        String querie = "insert into user values("+t.getIdUser()+
                ",'"+t.getNom()+
                "','"+t.getPrenom()+
                "','"+t.getRole()+
                "',"+t.getNumTel()+
                ",'"+"avatar"+
                "','"+t.getSexe().toString()+
                "','"+t.getEmail()+
                "','"+t.getPassword()+
                "','"+t.getLogin()+
                "','"+t.isIsBlocked()+"')";
        try {
            Statement stmt = cnx.createStatement();
            stmt.executeUpdate(querie);
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(User t) {
        String querie = "update user set "
                + "nom='"+t.getNom()+"',"
                + "prenom='"+t.getPrenom()+"',"
                + "role='"+t.getRole()+"',"
                + "telephone='"+t.getNumTel()+"',"
                + "avatar='av',"
                + "sexe='"+t.getSexe()+"',"
                + "email='"+t.getEmail()+"',"
                + "pwd='"+t.getPassword()+"',"
                + "login='"+t.getLogin()+"',"
                + "etat='"+t.isIsBlocked()+"'"
                +"where id_user="+t.getIdUser();
        try {
            Statement stmt = cnx.createStatement();
            stmt.executeUpdate(querie);
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void delete(User t) {
        String querie ="delete from user where id_user="+t.getIdUser();
        try {
            Statement stmt = cnx.createStatement();
            stmt.executeUpdate(querie);
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public User findById(Integer i) {
        String querie="select * from user where id_user="+i;
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);                         
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build();  
                
    }
    /*public HashSet<User> find(String args[],String vargs[])
	{ 
		HashSet<User> tvs=new HashSet<>();
		for(int i=0;i<args.length;i++)
		{
			switch (args[i]) {
			case "id":{
				int id =Integer.parseInt(vargs[i]);
				tvs.add(findById(id));
				return tvs;}
			case "nom":tvs.addAll(findByName(vargs[i]));
			case "prenom":tvs.addAll(findByPrenom(vargs[i]));
                        case "role":tvs.addAll(findByRole(vargs[i]));
                        case "numTel":tvs.addAll(findByNumTel(vargs[i]));
                        case "TypeSexe":tvs.addAll(findByTypeSexe(vargs[i]));
                        case "prenom":tvs.addAll(findByPrenom(vargs[i]));
                        case "prenom":tvs.addAll(findByPrenom(vargs[i]));
			default:
				break;
			}
                }
        }*/

    @Override
    public ArrayList<User> findByName(String val) {
        String querie="select * from user where nom='"+val+"'";
        ArrayList<User> list = new ArrayList<>();
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);           
                list.add(new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build());  
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

}
    

    @Override
    public ArrayList<User> findByPrenom(String val) {
        String querie="select * from user where prenom='"+val+"'";
        ArrayList<User> list = new ArrayList<>();
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);           
                list.add(new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build());  
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<User> findByRole(String val) {
        String querie="select * from user where role='"+val+"'";
        ArrayList<User> list = new ArrayList<>();
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);           
                list.add(new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build());  
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<User> findByEmail(String val) {
        String querie="select * from user where email='"+val+"'";
        ArrayList<User> list = new ArrayList<>();
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);           
                list.add(new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build());  
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<User> findByPassword(String val) {
        String querie="select * from user where password='"+val+"'";
        ArrayList<User> list = new ArrayList<>();
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);           
                list.add(new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build());  
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<User> findByLogin(String val) {
        String querie="select * from user where login='"+val+"'";
        ArrayList<User> list = new ArrayList<>();
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);           
                list.add(new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build());  
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<User> findByTypeSexe(TypeSexe val) {
        String querie="select * from user where sexe='"+val.toString()+"'";
        ArrayList<User> list = new ArrayList<>();
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);           
                list.add(new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build());  
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<User> findByNumTel(long num) {
        String querie="select * from user where numTel="+num;
        ArrayList<User> list = new ArrayList<>();
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);           
                list.add(new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build());  
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public ArrayList<User> findByIsBlocked(Boolean val) {
        String querie="select * from user where etat='"+val+"'";
        ArrayList<User> list = new ArrayList<>();
        int idUser=0;
        String nom="";
        String prenom="";
        String role="";
        long numTel=0;
        TypeSexe sexe=TypeSexe.Homme;
        String email="";
        String password="";
        String login="";
        boolean isBlocked=false;
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs=stmt.executeQuery(querie);
            while (rs.next()) {
                idUser=rs.getInt(1);
                nom=rs.getString(2);
                prenom=rs.getString(3);
                role=rs.getString(4);
                numTel=rs.getLong(5);
                sexe= rs.getString(7).equals("Homme") ? TypeSexe.Homme:TypeSexe.Femme;
                email=rs.getString(8);
                password=rs.getString(9);
                login=rs.getString(10);
                isBlocked=rs.getBoolean(11);           
                list.add(new User.Builder()
                    .email(email)
                    .idUser(idUser)
                    .login(login)
                    .nom(nom)
                    .numTel(numTel)
                    .password(password)
                    .prenom(prenom)
                    .role(role)
                    .state(isBlocked)
                    .build());  
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<User> getAll(User t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
