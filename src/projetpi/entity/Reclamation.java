/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.entity;

/**
 *
 * @author amin
 */
public class Reclamation {
    int idReclamation;
    int idUser;
    int description;

    @Override
    public String toString() {
        return "Reclamation{" + "idReclamation=" + idReclamation + ", idUser=" + idUser + ", description=" + description + '}';
    }

    public Reclamation(int idReclamation, int idUser, int description) {
        this.idReclamation = idReclamation;
        this.idUser = idUser;
        this.description = description;
    }
    
}
