/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.dao.daoInterfaces;

import java.util.ArrayList;
import projetpi.entity.User;
import projetpi.enums.TypeSexe;

/**
 *
 * @author amin
 */
public interface IUserDao extends IGeneralDao<User, Integer>{
    ArrayList<User> findByName(String nom);
    ArrayList<User> findByPrenom(String nom);
    ArrayList<User> findByRole(String nom);
    ArrayList<User> findByEmail(String nom);
    ArrayList<User> findByPassword(String nom);
    ArrayList<User> findByLogin(String nom);
    ArrayList<User> findByTypeSexe(TypeSexe nom);
    ArrayList<User> findByNumTel(long nom);
    ArrayList<User> findByIsBlocked(Boolean nom);
    
    
}
