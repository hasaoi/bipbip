/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.entity;

import projetpi.enums.TypeSexe;

/**
 *
 * @author amin
 */
public class User {
    private int idUser;
    private String nom;
    private String prenom;
    private String role;
    private long numTel;
    private TypeSexe sexe;
    private String email;
    private String password;
    private String login;
    private boolean isBlocked;
    
    public static class Builder{
    
        private int idUser;
        private String nom;
        private String prenom;
        private String role;
        private long numTel;
        private TypeSexe sexe;
        private String email;
        private String password;
        private String login;
        private boolean isBlocked;
       
        public User.Builder idUser(int val){
            this.idUser = val;
            return this;
        }
        
        
        public User.Builder nom( String val){
            this.nom = val;
            return this;
        }
        
        public User.Builder prenom(String val){
            this.prenom = val;
            return this;
        }
                
        public User.Builder role(String val){
            this.role = val;
            return this;
        }

        
        public User.Builder numTel( long val){
            this.numTel = val;
            return this;
        }

        public User.Builder sexe(TypeSexe val){
            this.sexe = val;
            return this;
        }

        public User.Builder email(String val){
            this.email = val;
            return this;
        }
        
        public User.Builder password(String val){
            this.password = val;
            return this;
        }    

        public User.Builder login(String val){
          this.login = val;
          return this;
        }

        public User.Builder state(Boolean val){
          isBlocked = val;
          return this;
        }
        public User build(){
          return new User(this);
        }
  }
    private User(User.Builder builder){
    idUser = builder.idUser;
    nom = builder.nom;
    prenom = builder.prenom;
    role = builder.role;
    numTel = builder.numTel;
    sexe = builder.sexe;
    email = builder.email;
    password = builder.password;
    login = builder.login;
    isBlocked = builder.isBlocked;
    
    
    
  }


    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public long getNumTel() {
        return numTel;
    }

    public void setNumTel(long numTel) {
        this.numTel = numTel;
    }

    public TypeSexe getSexe() {
        return sexe;
    }

    public void setSexe(TypeSexe sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    @Override
    public String toString() {
        return "User{" + "idUser=" + idUser + ", nom=" + nom + ", prenom=" + prenom + ", role=" + role + ", numTel=" + numTel + ", sexe=" + sexe + ", email=" + email + ", password=" + password + ", login=" + login + ", isBlocked=" + isBlocked + '}';
    }
    
}
