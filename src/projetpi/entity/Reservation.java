/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.entity;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author USER
 */
public class Reservation {
  private int id_rec ;
  private  int id_user;
  private  int id_voyage ;
  private  Timestamp date_creation ;
  private  int etat ;
  private  int nb_place_des;

    public Reservation(int id_user, int id_voyage, Timestamp date_creation, int etat, int nb_place_des) {
        
        this.id_user = id_user;
        this.id_voyage = id_voyage;
        this.date_creation = date_creation;
        this.etat = etat;
        this.nb_place_des = nb_place_des;
    }

    public void setId_rec(int id_rec) {
        this.id_rec = id_rec;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public void setId_voyage(int id_voyage) {
        this.id_voyage = id_voyage;
    }

    public void setDate_creation(Timestamp date_creation) {
        this.date_creation = date_creation;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public void setNb_place_des(int nb_place_des) {
        this.nb_place_des = nb_place_des;
    }

    public Reservation() {
    }

    public int getId_rec() {
        return id_rec;
    }

    public int getId_user() {
        return id_user;
    }

    public int getId_voyage() {
        return id_voyage;
    }

    public Timestamp getDate_creation() {
        return date_creation;
    }

    @Override
    public String toString() {
        return "Reservation{" + "id_rec=" + id_rec + ", id_user=" + id_user + ", id_voyage=" + id_voyage + ", date_creation=" + date_creation + ", etat=" + etat + ", nb_place_des=" + nb_place_des + '}';
    }

    public int getEtat() {
        return etat;
    }

    public int getNb_place_des() {
        return nb_place_des;
    }
    
    
    
    
    
    
}
